<?php

/**
 * @file
 * Hooks provided by Video Sitemap module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the list of Node IDs to be added to queue.
 *
 * @param array $nids
 *   The list of all nodes to be added to the Video Sitemap queue.
 */
function hook_video_sitemap_queue_items_alter(array &$nids) {
  foreach ($nids as $index => $nid) {
    if ($nid === '123') {
      unset($nids[$index]);
    }
  }
}

/**
 * @} End of "addtogroup hooks".
 */
